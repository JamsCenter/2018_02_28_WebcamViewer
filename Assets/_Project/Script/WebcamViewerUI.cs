﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcaùViowerUI : MonoBehaviour
{
    public RawImage rawimage;
    public AspectRatioFitter ratioFitter;
    public Texture webcamTexture;


    private void Update()
    {
        ratioFitter.aspectRatio = (float)webcamTexture.width / (float)webcamTexture.height;
        Debug.Log(string.Format("{0}x{1}", webcamTexture.width, webcamTexture.height));
    }

    public void SetWithWebcam(int index)
    {


    }

    public void SetWithWebcam(WebCamDevice device)
    {
        WebCamTexture webcamTexture = new WebCamTexture(device.name);
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        this.webcamTexture = webcamTexture;
        Debug.Log(string.Format("{0}x{1}", webcamTexture.width, webcamTexture.height));
        webcamTexture.Play();
        DisplayWebcameName();

    }
    public static void DisplayWebcameName()
    {
        WebCamDevice[] cam_devices = WebCamTexture.devices;

        for (int i = 0; i < cam_devices.Length; i++)
        {
            print("Webcam available: " + cam_devices[i].name);
        }

    }
}

