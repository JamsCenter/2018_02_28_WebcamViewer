﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebcamNumberToPanel : MonoBehaviour {

    public GameObject[] _panelsByCamera;

	// Use this for initialization
	void Awake () {

        for (int i = 0; i < _panelsByCamera.Length; i++)
        {
            _panelsByCamera[i].SetActive(false);

        }
        Debug.Log("Webcam number:" + WebCamTexture.devices.Length);
        _panelsByCamera[WebCamTexture.devices.Length -1].SetActive(true);


    }
	

}
